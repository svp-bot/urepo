#!/bin/bash
#
# Script to update a set of minimal Debian package repositories
# (useful to support, for example, a one-repository-per-project model).
#
# Invoke this tool from the top-level directory of your package
# archive, which should have a separate subdirectory for each
# repository.
#
# Set the KEYID environment variable if you want to sign the
# release files with a non-default GPG key.
#

unset LANG

# Hashes that should be provided in the Release file.
# Format is <name>:<program_to_invoke>.
release_hashes="MD5Sum:md5sum SHA1:sha1sum SHA256:sha256sum"

usage() {
    cat <<EOF
Usage: $(basename "$0") [<OPTIONS>] [<DIR>, ...]

\`urepo' (micro-repo) is a tool to maintain one or more minimal Debian
package repositories, for easy publication over HTTP. Useful, for
example, to support small projects that need a package repository.

A repository here is just a directory containing Debian packages.
Passing a path to such a directory to \`urepo' will generate new
(gpg-signed) Packages and Release files for clients to download. All
that is necessary to export the package repository is for that
directory to be remotely accessible over HTTP.

If you need to update more than one repository, just pass multiple
paths as command line arguments.

Release files will be signed by default (using the \`gpg' tool), so
make sure the relevant PGP key is available. You can choose which key
to use with the \`--keyid' option.

Known options:

  -k, --keyid=KEYID    Sign release files with this key.

  -u, --no-sign, --unsigned 
                       Do not sign release files.

  --origin=ORIGIN      Set repository Origin field (default: urepo).

EOF
}

# Parse command-line options.
keyid=
sign=y
origin=urepo

while :; do
    case "$1" in
        -h|--help)
            usage
            exit 0
            ;;

        -k|--keyid)
            if [ -n "$2" ]; then
                keyid="$2"
                shift 2
                continue
            else
                echo "Error: --keyid requires an argument" >&2
                exit 1
            fi
            ;;
        --keyid=*)
            keyid="${1#*=}"
            if [ -z "${keyid}" ]; then
                echo "Error: --keyid requires an argument" >&2
                exit 1
            fi
            ;;

        --origin)
            if [ -n "$2" ]; then
                origin="$2"
                shift 2
                continue
            else
                echo "Error: --origin requires an argument" >&2
                exit 1
            fi
            ;;
        --origin=*)
            origin="${1#*=}"
            if [ -z "${origin}" ]; then
                echo "Error: --origin requires an argument" >&2
                exit 1
            fi
            ;;

        --sign)
            sign=y
            ;;
        -u|--no-sign|--nosign|--unsigned)
            sign=n
            ;;

        --)
            shift
            break
            ;;
        -?*)
            usage
            echo "Error: unknown option '$1'" >&2
            exit 1
            ;;

        *)
            break
    esac

    shift
done

# Find all unique architectures present in a repository.
find_repo_archs() {
    find "$1" -type f -name '*.deb' -printf '%f\n' \
        | sed -e 's/^.*_\(.*\)\.deb/\1/' \
        | sort -u
}

# Create a Release file.
make_release() {
    local dist="$1"
    cd "${dist}"

    local archs=$(find_repo_archs .)
    echo "Origin: ${origin}"
    echo "Label: Debian packages"
    echo "Suite: unstable"
    echo "Codename: ${dist}"
    echo "Date: $(date --rfc-2822)"
    echo "Architectures:" ${archs}
    echo "Components: main"
    echo "Description: A repository of Debian packages."
    for hashspec in ${release_hashes} ; do
        name=${hashspec%%:*}
        hashprog=${hashspec##*:}
        echo "${name}:"
        for f in Packages* ; do
            h=$(${hashprog} "${f}" | awk '{print $1}')
            size=$(stat -c %s "${f}")
            echo " ${h} ${size} ${f}"
        done
    done
}

# Sign a Release file.
sign_release() {
    [ "${sign}" = "y" ] || return
    local file="$1"
    local gpgopts=
    [ -n "${keyid}" ] && gpgopts="-u ${keyid}"
    gpg ${gpgopts} --sign --digest-algo SHA512 --detach --armor "${file}"
    if [ $? -gt 0 ]; then
        echo "Error signing the release file" >&2
        exit 2
    fi
    mv -f "${file}.asc" "${file}.gpg"
}

if [ $# -eq 0 ]; then
    echo "Error: no repositories specified." >&2
    exit 1
fi
for dir in "$@" ; do
    [ -d "${dir}" ] || continue
    (if ! flock --wait 300 9 ; then
        echo "Could not acquire lock on ${dir}" >&2
        continue
     fi
     dpkg-scanpackages -m "${dir}" > "${dir}/Packages" \
        && bzip2 -kf "${dir}/Packages" \
        && (make_release "${dir}") > "${dir}/Release" \
        && sign_release "${dir}/Release"
    ) 9>${dir}/.lock
done

exit 0
