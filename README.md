
μrepo
=====

*μrepo* (`urepo`) is a small tool to maintain simple Debian package
repositories, for easy publication over HTTP. Useful, for example, to
support small projects that need a package repository.

A repository here is just a directory containing Debian packages.
Passing a path to such a directory to `urepo` will generate new
(gpg-signed) Packages and Release files for clients to download. All
that is necessary to export the package repository is for that
directory to be remotely accessible over HTTP.

If you need to update more than one repository, just pass multiple
paths as command line arguments.

Release files will be signed by default (using the `gpg` tool), so
make sure the relevant PGP key is available. You can choose which key
to use with the `--keyid` option.


## Installation

Just clone this repository and copy the `urepo` script somewhere like
`/usr/local/bin`:

    $ git clone https://git.autistici.org/ai3/tools/urepo
    $ sudo cp urepo/urepo /usr/local/bin

The script, beyond standard tools which should be available on any
system, depends on the following software:

* GNU coreutils (more specifically, the `md5sum`, `sha1sum` and
  `sha256sum` suite of tools);
* GnuPG, to sign the repositories (if desired);
* `bzip2`
* `dpkg-scanpackages`, which is part of the `dpkg-dev` package on
  Debian-based systems.


## Usage

Assuming that the directory `/home/myproject/repo` (just to make an
example) contains some Debian packages, running the following command
will generate the necessary repository metadata:

    $ urepo /home/myproject/repo

Assume also that a webserver on this same machine is configured to
serve the `/debian/myproject` URL from the `/home/myproject/repo`
directory. Then, clients wanting to use this repository can put the
following line in their `sources.list` file:

    deb http://my.server/debian myproject/

The distribution path field must end with a slash. This also provides
the name of the distribution (to use for instance with `apt-get
install -t`).


### Slightly less manual setup

If you happen to do this sort of thing regularly, or you'd like a
slightly higher level of automation, you can use the `urepo-put` tool
and its counterpart, `urepo-ssh-handler`. Let's assume the following:

* you have a centralized repository for your projects on a server
  somewhere
* you have created a dedicated user for this purpose on that server
* you build your packages on a different host, so you'd like to use
  SSH for authentication and transport

You can then add your SSH key to the repository user's
`.authorized_keys` file, configuring `urepo-ssh-handler` as the forced
command:

    command="urepo-ssh-handler --root=/var/repo" ssh-rsa AA....

This will allow you to use, on your host, a command such as:

    urepo-put --repo=NAME package.deb

and have your packages automatically end up in `/var/repo/NAME` on the
server, with automatic update of the repository metadata.
